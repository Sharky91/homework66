class UsersController < ApplicationController
  def show
  	@user = User.find(params[:id])
  	@posts = Post.all
  end

  def follow
  	@user = User.find(params[:id])
  	
    if current_user.id == @user.id
      redirect_to user_path(@user), notice: 'Вы не можете подписаться на себя'
    else
      if current_user.followers.exists?(friend_id: @user.id)
        redirect_to user_path(@user), notice: 'Вы подписаны на этого пользователя'
      else
        @follow = current_user.followers.build
        @follow.friend_id = @user.id

        if @follow.save
          redirect_to user_path(@user), notice: 'Вы подписаны на этого пользователя'
        end
      end
    end
  end

  def unfollow
  	@user = User.find(params[:id])
    if current_user.id == @user.id
      redirect_to user_path(@user), notice: 'Вы не можете подписаться на себя'
    else
      if current_user.followers.exists?(friend_id: @user.id)
        @follow = current_user.followers.find_by(friend_id: @user.id)
        @follow.destroy
        redirect_to user_path(@user), notice: 'Вы больше не подписаны на этого пользователя'
      else
        redirect_to user_path(@user), notice: 'Вы не были подписаны на этого пользователя'
      end
    end
  end

  
end
