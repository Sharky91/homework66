class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable
  
  validates :name, presence: true, length: { maximum: 50 }
  has_one_attached :avatar

  has_many :posts
  has_many :likes, dependent: :destroy
  has_many :comments
  has_many :followers, dependent: :destroy


end
