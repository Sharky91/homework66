Rails.application.routes.draw do
	devise_for :users
	
	resources :users, :only => [:show]

	get 'users/:id/follow' => 'users#follow', as: 'follow'
	get 'users/:id/unfollow' => 'users#unfollow', as: 'unfollow'

	root 'posts#index'
	
	resources :posts do
		resources :likes, :only => [:create, :destroy]
		resources :comments, :only => [:create]
	end

end
